# LifeApps

## Desafio de selecao - Dev JS

O restaurante do Sr Pedro quer ajudar seus clientes a elaborar seus pedidos de marmita. No seu restaurante existe um cardápio que segue as seguintes regras:

- Existem pratos fixos, disponíveis todos os dias
- Para cada dia da semana em que o restaurante está aberto alguns pratos extras estão disponíveis, formando um cardápio variável

Os clientes do Restaurante do Sr Pedro querem uma forma simples de escolher os ingredientes da sua entrega. Para ajudar os clientes, sua esposa, Dona Augusta, elaborou uma ideia e a disponibilizou no link [Gerador de Almoço - versão 1](https://dev-superon-public-media.s3-sa-east-1.amazonaws.com/selecao-novatos/mock-gerador-marmita/Prototype1/index.html#/screens/d12245cc-1680-458d-89dd-4f0d7fb22724).

Dona Augusta também elaborou um JSON com o cardápio, para centralizar a forma como esses dados estão disponíveis. Ela também colocou a bebida do dia, mas esqueceu de adicionar ao design e resolveu confiar no desenvolvedor do projeto. Os dados estão disponíveis em [Repositorio de Cardápio](https://dev-superon-public-media.s3-sa-east-1.amazonaws.com/selecao-novatos/api-prova-selecao.json)

Dona Augusta atribuiu para você a task de desenvolver a tela na linha do protótipo, seguindo as diretivas à seguir (todas obrigatórias):

- No máximo 3 Carbos, 3 Proteinas e 3 Vegetais
- Um máximo de 7 ingredientes por marmita
- Ela quer mostrar a bebida do dia com algum destaque
- Deve ser feito um sorteio entre os itens do cardápio fixo e os itens do cardápio variável do dia.
- A cada vez que o botão de gerar a marmita for pressionado um novo sorteio deve ser feito
- Não deve ser utilizado JQuery. A escolha de framework é livre.

Além das diretivas obrigatórias, é desejável, porém não fundamental:
- Criar a capacidade de gravar as marmitas geradas no passado, dando ao usuário a possibilidade de ver os resultados passados. Isso deve ser gravado em algum banco de dados
- O backend para que isso aconteça deve ser construido em NodeJs, obrigatoriamente.
- O banco é da escolha do desenvolvedor.
- O design dessa exibição é livre para que o desenvolvedor exerça sua liberdade criativa.
- Envolvendo o banco é desejável criar um docker-compose para que os testes possam ser feitos com fidelidade

NOTAS:
- As tecnologias foco de avaliação são NodeJs, React/Vue, Docker e Banco relacional. O desafio deixa alguns desses itens em aberto ou opcionais para verificar a familiaridade com tecnologias modernas de front-end



